# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header:$

inherit font

DESCRIPTION="Linotype's Palatino font"
HOMEPAGE="http://www.linotype.com/1317/palatino-family.html"
SRC_URI=""
LICENSE="MSttfEULA"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86 ~x86-fbsd"

S=${WORKDIR}
FONT_S=${WORKDIR}
FONT_SUFFIX="ttf"
FONTDIR="/usr/share/fonts/palatino"
MYDISTDIR="/usr/portage/distfiles"

src_unpack() {

	ls ${MYDISTDIR}/pala.ttf >/dev/null || die "Get pala.ttf and put it in ${MYDISTDIR}"
	ls ${MYDISTDIR}/palai.ttf >/dev/null || die "Get palai.ttf and put it in ${MYDISTDIR}"
	ls ${MYDISTDIR}/palab.ttf >/dev/null || die "Get palab.ttf and put it in ${MYDISTDIR}"
	ls ${MYDISTDIR}/palabi.ttf >/dev/null || die "Get palabi.ttf and put it in ${MYDISTDIR}"
	cp ${MYDISTDIR}/pala.ttf ${MYDISTDIR}/palai.ttf  \
		${MYDISTDIR}/palab.ttf ${MYDISTDIR}/palabi.ttf ${S}
}
