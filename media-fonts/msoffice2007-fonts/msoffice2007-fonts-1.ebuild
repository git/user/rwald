# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header:$

inherit font

DESCRIPTION="Microsoft fonts introduced with Office 2007"
HOMEPAGE="http://microsoft.com"
SRC_URI="msoffice2007-fonts-1.tar.bz"
LICENSE="MSttfEULA"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86 ~x86-fbsd"

S=${WORKDIR}
FONT_S=${WORKDIR}
FONT_SUFFIX="ttf"
FONTDIR="/usr/share/fonts/msoffice2007-fonts"
