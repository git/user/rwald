# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit rpm versionator

MY_PV="$(get_version_component_range 1-3)"
S="${WORKDIR}"

DESCRIPTION="Image Scan! for Linux network plugin package"
HOMEPAGE="http://avasys.jp/english/linux_e/"
SRC_URI="x86?  ( http://linux.avasys.jp/drivers/scanner-plugins/${PN}/${MY_PV}/${PN}-$(replace_version_separator 3 -).i386.rpm )
		 amd64?  ( http://linux.avasys.jp/drivers/scanner-plugins/${PN}/${MY_PV}/${PN}-$(replace_version_separator 3 -).x86_64.rpm )"

LICENSE="GPL-2 LGPL-2 AVASYS EPSON"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="media-gfx/iscan"
RDEPEND="${DEPEND}"

src_install() {
	dodoc ${S}/usr/share/doc/${PN}-${MY_PV}/README ${S}/usr/share/doc/${PN}-${MY_PV}/NEWS

	use x86 && MY_ARCH=""
	use amd64 && MY_ARCH="64"
	insinto /usr/lib${MY_ARCH}/iscan/
	doins ${S}/usr/lib${MY_ARCH}/iscan/network
}
