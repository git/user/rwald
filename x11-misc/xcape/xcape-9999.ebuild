# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4
inherit git-2

DESCRIPTION="Daemon to make Ctrl key also function as Esc"
HOMEPAGE="https://github.com/alols/xcape"
SRC_URI=""
EGIT_REPO_URI="git://github.com/alols/xcape.git
	https://github.com/alols/xcape.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""
DOCS=( README.md )

DEPEND="x11-libs/libX11
	x11-libs/libXtst"
RDEPEND="${DEPEND}"
